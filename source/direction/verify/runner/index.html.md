---
layout: markdown_page
title: "Category Vision - Runner"
---

- TOC
{:toc}

## GitLab Runner

The GitLab Runner is our execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### The Custom Executor

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by recently adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html)
that can be overridden to support different needs. In this way platforms like Lambda, z/OS, vSphere, and
even custom in-house implementations can be supported in the runner for your needs.

## What's Next & Why

Up next is the beta launch of [Windows Shared Runners](https://gitlab.com/gitlab-org/gitlab-runner/issues/4815) on GitLab.com  This will gives users the tooling to build their Windows projects on GitLab.com without needing to setup and configure their own Windows build machines.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. It's important to us that
we defend our lovable status, though, so if you see any actual or potential gap please let us know in
our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) for this category.

Maturing the Windows executor is of particular importance as we improve our support for Windows
across the board. There are a few issues we've identified as being critical to that effort:

- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Support services on Windows Docker containers](https://gitlab.com/gitlab-org/gitlab-runner/issues/4186)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

For the moment, the Runner is evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

We have a few top issues that we're investigating:

- [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797): Local runner execution
- [gitlab-runner#2229](https://gitlab.com/gitlab-org/gitlab-runner/issues/2229): Adding Services With Kubernetes Executor
- [gitlab-runner#2007](https://gitlab.com/gitlab-org/gitlab-runner/issues/2007): Variables nested substitution
- [gitlab-runner#1736](https://gitlab.com/gitlab-org/gitlab-runner/issues/1736): File/directory creation umask when cloning is `0000`

## Top Internal Customer Issue(s)

There are a few top internal customer issues that we're investigating :

- [gitlab-runner#1042](https://gitlab.com/gitlab-org/gitlab-runner/issues/1042): Create network per build to link containers together
- [gitlab#25969](https://gitlab.com/gitlab-org/gitlab/issues/25969): Allow advanced configuration of GitLab runner when installing GitLab managed apps

## Top Vision Item(s)

We frequently receive requests for supporting different provisioning platforms and
architectures in the runner - we haven't been able to support these because each combination
brings in additional requirements for testing and maintenance. Instead, we want to enable
people to build their own integration steps into the runner in a safe way. Through introducing
a shim for the core runner capabilites in [gitlab-runner#2885](https://gitlab.com/gitlab-org/gitlab-runner/issues/2885)
we make this possible for our users.

Additionally, supporting GitLab runners on IBM z/OS for IBM mainframes, [gitlab-runner#3263](https://gitlab.com/gitlab-org/gitlab-runner/issues/3263) is an issue that is gaining interest from the community.
