---
layout: markdown_page
title: "Internship Pilot Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 2, 2019 |
| Target End Date | November 1, 2019 for Primary exit criteria, 30 November for Secondary criteria |
| Slack           | [#wg_internship-pilot](https://gitlab.slack.com/archives/CNLLV1NEN/) (only accessible from within the company) |
| Google Doc      | [Internship Working Group Agenda](https://docs.google.com/document/d/1KsdRtkRcF4EOpL2s0JDC-th8SB7nHstw8cfceLrzG-o/edit#) (only accessible from within the company) |
| Issue Board     | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/1360344?&label_name[]=wg-internship-pilot)             |

## Business Goal

Run a pilot internship program to determine feasibility for future programs.

Overview:
1. Intention of hiring available candidates that perform well at the end of the internship
1. Limit duration (see exit criteria)
1. Open to anyone starting their career in tech (not just students)
1. Timeline:
    1. Design in Oct
    1. Recruiting in Nov
    1. Offers in Dec
    1. Internship start Summer 2020
1. We welcome applications from anyone. 100% of the active sourcing our recruiting department will do will be from groups under-represented in the technology industry. This is in order to make the candidate pool more reflective of society at large. Offers will be made purely based on merit.

## Exit Criteria (~68%)

Due to the need to move quickly the exit criteria is grouped in primary and secondary with the former needing to be completed to start engaging candidates.

### Primary (~81%)
1. [Define budget](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5526) => `100%`
    1. Determine number of internship positions available ✅
    1. Determine compensation ✅
    1. Determine budget for co-location ✅
1. [Communication plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5527) => `66%`
    1. Choose the internship program name ✏️
    1. Define branding opportunities for all-remote company culture ✏✅
    1. Define communication milestones ✏✅
1. [Recruitement plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5528) => `75%`
    1. Define candidate qualification criteria ✅ 
    1. Create job family page ✅
    1. Identify sourcing targets and engagement plan ✅   
    1. Create Greenhouse vacancies ☑️
1. [Internship program structure](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5529) => `100%`
    1. Determine duration of the internship ✅
    1. Determine timing of the program ✅
    1. Decide on all-remote vs timezone alignment vs co-located cohort ✅
    1. Define the criteria for teams to be able to request an intern slot ️️✅
    1. Define expected day-to-day activities for interns (skeleton outline) ✅
1. [Interviewing](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5530) => `66%`
    1. Define interview process and technical assessments ️️✏️

### Secondary (55%)
1. [Onboarding/offboarding](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5531) => `10%`
    1. Determine onboarding process ✏️
    1. Determine offboarding communication to interns ️☑️
1. [Measurements of success](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5532) => `100%`
    1. Determine criteria for considering the pilot a success ✅

 (✅ Done, ✏️ In-progress, ☑️ Still to be done)   

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Eric Johnson          | VP of Engineering              |
| Facilitator           | Jean du Plessis       | Frontend Engineering Manager   |
| Functional Lead       | Roos Takken           | People Business Partner, Engineering |
| Functional Lead       | Nick Nguyen           | Engineering Manager, Ecosystem |
| Member                | Tanya Pazitny         | Quality Engineering Manager    |
| Member                | Clement Ho            | Frontend Engineering Manager   |
| Member                | Phil Calder           | Growth Engineering Manager     |
| Member                | Liam McNally          | Interim Recruting Manager      |
| Member                | John Hope             | Engineering Manager, Plan      |

---

## Internship Pilot Proposal

### Overview
GitLab would like to establish an on-going internship program that can offer students, graduates, career switchers and those returning to the workforce an opportunity to further their career and it is our desire to create an inclusive program that will provide fair and equal opportunity for all qualifying candidates.

Due to the fact that we do not yet have an established program that has been tried and tested we are undertaking a limited pilot to validate and refine our approach to offering internships at an all-remote company.
For this reason the structure and scope of this program will likely change in the future.

### Candidate qualifying criteria
The following criteria are considered required for candidates to be eligible for the pilot internship program:
1. Ruby on Rails and/or modern frontend JavaScript framework (React, VueJS, AngularJS, Ember etc.) programming experience.
1. Contributor to open source projects
1. Available full-time during the course of the internship
1. Able and willing to travel to the United States of America and the Netherlands for 1 week each
1. Interested in fulltime employment after the internship (either immediately, or "next semester") - no upfront commitment to GitLab required

### Program Structure

#### Duration and Timing
We want to make this program available to anyone who is ready to begin a career in software development.  
A priority for the pilot program is attracting promising university students who are nearing graduation.  
Therefore, we are choosing dates that align with university academic calendars.

1. 9 week program starting Mon, June 15, 2020 until Friday 14, August 2020.

#### Location
The internship program will primarily be remote, with two weeks of in-person co-location.

1. Week 1: Intern Fast Boot
    - Interns will kick-off the program in person. 
    - Amsterdam is the proposed location as it is home to many GitLabbers and is an attractive destination to help us promote the program.
    - 1-2 program coordinators will be present to assist with onboarding, support, and to organize intern social events.
    - Mentors will also be on-site to support the interns
1. Weeks 2-8: Remote internship   
    - Interns will work remotely in their assigned team
1. Week 9: Intern send-off 
    - Interns meet for their final week to process their experience and socialize with each other as well as with GitLab coordinators and mentors.
    - San Francisco is the proposed location. Like Amsterdam, it is an attractive destination that is also home to many GitLab team members. 
    - We would also like to arrange for the interns to meet with Sid if possible.
1. GitLab Contribute Invite
    - We will extend invites to attend Contribute in March. 
    - We don’t expect that all interns will be able to attend and will only plan intern specific events if most are able to attend.    

#### Criteria for Requesting an Intern
We will create a Google Form for teams that wish to request an intern.  

The form will evaluate the following criteria:
1. Mentorship
    - Does the team have a manager and at least 1-2 engineers who are willing to serve as mentors for the duration of the program?
    - Do the mentors have previous experience mentoring interns or junior engineers? Previous experience is a nice-to-have, but not a must-have.
1. Workload
    - Does the team have a roadmap containing low weight issues with few dependencies that would be appropriate for an intern to work on?
1. Team Size and Maturity
    - How established is this team? Will they be able to take on an intern without risking a decrease in velocity?     

Team applications will be evaluated and approved/rejected by the Program coordinator, VP of Engineering and the HR Business Partner for Engineering.

#### Internship day-to-day activities
1. The activities during the co-located weeks (1 & 9) are still to be determined.
1. For weeks 2-8 an intern's daily schedule will generally reflect how GitLab team members work, which is to say we will not impose a rigid schedule. 
    - Interns will be encouraged to favor async communication and to set their own work schedule. 
    - Mentors and program coordinators will provide coaching if an intern needs help adjusting to remote work.
1. Interns will participate in the following pre-determined activities
    - Weekly 1:1 with their manager
    - Weekly 1:1 with a mentor
    - Weekly 1:1 with an internship program coordinator
    - Weekly intern coffee chat
    - 2-3 group meetings per week moderated by the program coordinator, rotating in timezone assuming we have people in more than one timezone.
    - Regular pair programming sessions with mentor and other team members.

### Recruitment 
For the pilot we will primarily target our advertising and promotion of the program at university students, however, we are not limiting candidate intake to university students and are open to all qualifying candidates.

#### Advertising
1. Advertise the roles across all traditional platforms
1. Advertise on internship focussed job boards (e.g. Youtern, angel.co etc)

#### University Recruitment
1. Proactively reach out to and engage with universities/colleges to hold a Virtual Careers Talk to encourage students at those universities to apply.
1. Proactively reach out too and engage with any associations within these universities that represent underrepresented groups and hold a Virtual Careers Talk, to encourage students to apply.
1. Hold virtual talks at 5 universities in the following regions: North America, European, APAC, MENA

#### Underrepresented groups
1. All our active sourcing activities will exclusively be focused on candidates from underrepresented groups.
1. We will be utilizing LinkedIn, meetup groups and online tech forums to identify potential candidates.

### Interviewing
We anticipate a high volume of applications and as such we are setting the bar high at the application stage and introducing an automated online assessment to help with screening candidates.
These steps will allow us to work with a manageable number of candidates during the face-to-face interview stages.

Interview process steps:
1. Comprehensive Application form including
    - Technical assignment
    - Situational questions rather than STAR, as may not have any direct work/relatable experience.
    - Cover letter asking for candidates to specifically address defined areas.
1. Automated online technical assessment
1. Technical assessment interview \*
1. Behavioral/Values interview panel \*\*
    - Hiring manager
    - Recruiter/HR
    - Misc Interviewer

\* To be confirmed whether this step will be included  
\*\* Candidates must score a strong yes to move on to the panel interview stage.

### Communications plan
We want to use this opportunity to further the branding of GitLab's all-remote company culture.

#### Program name
1. To be determined, but initial ideas include:
    - "Commit to Coding"
    - "Remote Summer Internship"
    - "Your First Software Engineering Job"

#### Branding opportunities
1. Connect with Universities
1. Connect with Meetup groups
1. Connect with Programming bootcamps
1. LinkedIn, Instagram, FB ads
1. Sourcing
1. Gitlabbers to contact their universities/Meetup groups/programming clubs

#### Communication milestones
1. 2019-11-05: Internal announcement of the program + handbook update
1. 2019-11-15: Opening of jobs (Glassdoor, LinkedIn, Stack Overflow etc.)
1. 2019-11-30 - 2019-12-15: Internship branding campaign
1. 2020-01-01 - 2020-02-01: Announce who will be invited/join contribute.
1. 2020-03-27: Contribute blog on interns
1. 2020-06-06: Blog on kickoff + Personal stories
1. 2020-08-14: Evaluation blog post.
1. 2020-10/2020-11: Announce next internship (if pilot was successful)

### Budget
Due to the confidential nature of this information please see details in this issue: [!5526](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5526) (private to GitLab)

### Measuring Success
We will use the following criteria to determine whether this pilot was successful and whether to consider further rollout/expansion in the future.

1. Feedback rating on the program experience as scored by the interns (questions and desired score TBD).
1. Feedback rating on the value and overall experience working with the intern from the teams with interns (questions and desired score TBD).
1. Throughput of at least 14 MRs over the period of the internship
    - Average of 2 MRs merged per week
    - This takes into consideration the 9 week internship with 1 week onboarding, 1 week offboarding
    - Handbook/Blog MRs will count towards throughput
1. Making a job offer(s) to 50% of the interns in the program